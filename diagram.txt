
                            +-------------+             -------------
                            | FOREIGN     |          --/             \--
                            |             |         /                   \
                            |     10.0.0.1|        (      INTERNET       )
                            |         eth0+---------+      (fake)       /
                            +-------------+          --\             /--
                                                        ------+------
                                                              |
                                                              |
                                                       +------+-------+
                                                       |    eth1      |
 +-------------+   +-------------+   +-------------+   |  10.0.0.1    |
 | IDS         |   | WEB         |   | PROXY       |   |              |
 |             |   |             |   |             |   |   ROUTER1    |
 | 172.16.10.4 |   | 172.16.10.5 |   | 172.16.10.6 |   |              |
 |    eth0     |   |    eth0     |   |    eth0     |   | 172.16.10.1  |
 +------+------+   +------+------+   +-----+-------+   |    eth0      |
        |                 |                 |          +------+-------+
        |                 |                 |                 |
        |                 |                 |                 |
  +-----+-----------------+-----------------+-----------------+------+
  |                                                                  |
  |                    DMZNET 172.16.10.0/24                         |
  |                                                                  |
  +--------------------------------+---------------------------------+
                                   |
                                   |
                                   |
                            +------+-------+
                            |    eth0      |
                            | 172.16.10.2  |
                            |              |
                            |   ROUTER2    |
                            |              |
                            | 172.16.12.1  |
                            |    eth1      |
                            +------+-------+
                                   |
                                   |
                                   |
  +--------------------------------+---------------------------------+
  |                                                                  |
  |                   INTRANET 172.16.12.0/24                        |
  |                                                                  |
  +-----+-----------------+-----------------+-----------------+------+
        |                 |                 |                 |
        |                 |                 |                 |
        |                 |                 |                 |
 +------+------+   +------+------+   +------+------+   +------+------+
 |    eth0     |   |    eth0     |   |    eth0     |   |    eth0     |
 | 172.16.12.3 |   | 172.16.12.4 |   | 172.16.12.5 |   | 172.16.12.6 |
 |             |   |             |   |             |   |             |
 | HOSTA       |   | HOSTB       |   | HOSTC       |   | HOSTD       |
 +-------------+   +-------------+   +-------------+   +-------------+

docker exec ids ip route del default
docker exec ids ip route add default via 172.16.10.1
docker exec web ip route del default
docker exec web ip route add default via 172.16.10.1
docker exec proxy ip route del default
docker exec proxy ip route add default via 172.16.10.1
docker exec hostA ip route del default
docker exec hostA ip route add default via 172.16.12.1
docker exec hostB ip route del default
docker exec hostB ip route add default via 172.16.12.1
docker exec hostC ip route del default
docker exec hostC ip route add default via 172.16.12.1
docker exec hostD ip route del default
docker exec hostD ip route add default via 172.16.12.1





