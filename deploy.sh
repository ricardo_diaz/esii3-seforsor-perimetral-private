#!/usr/bin/env bash

set -e

delete_docker_postrouing_on_host() {
    local cidr=$1
    iptables -tnat -D POSTROUTING $(iptables -tnat -L -n --line | egrep "MASQUERADE.*${cidr}" | awk '{print $1}')
}

route_add() {
    local container_name="$1"
    local target_net="$2"
    local gateway="$3"
    docker exec "$container_name" ip route add "$target_net" via "$gateway"    
}

# Deploy containers and networks
docker-compose up -d --build

# Docker do not interfere my neworks
docker_network_cidrs=("10.0.0.0/8" "172.16.10.0/24" "172.16.12.0/24")
for docker_network_cidr in "${docker_network_cidrs[@]}"
do
    delete_docker_postrouing_on_host "$docker_network_cidr"
done

# Configure routes to internet (our fake Internet) from DMZ nodes
dmz_nodes=("ids" "web" "proxy")
for container_name in "${dmz_nodes[@]}"
do
    route_add "$container_name" "10.0.0.0/8" "172.16.10.1"
done

# Configure routes to internet (our fake Internet) from intranet nodes
intranet_nodes=("hostA" "hostB" "hostC" "hostD")
for container_name in "${intranet_nodes[@]}"
do
    route_add "$container_name" "10.0.0.0/8" "172.16.12.1"
done
