#!/usr/bin/env bash

set -e

router1 () {
    docker exec router1 "$@"
}

reset_default_route() {
    local container_name="$1"
    local gateway="$2"
    docker exec "$container_name" ip route del default
    docker exec "$container_name" ip route add default via "$gateway"    
}

delete_docker_postrouing_on_host() {
    local cidr=$1
    iptables -tnat -D POSTROUTING $(iptables -tnat -L -n --line | egrep "MASQUERADE.*${cidr}" | awk '{print $1}')
}

#
# Deploy containers and networks
#
docker-compose up -d
# Docker do not interfere my neworks
docker_network_cidrs=("10.0.0.0/8" "172.16.10.0/24" "172.16.12.0/24")
for docker_network_cidr in "${docker_network_cidrs[@]}"
do
    delete_docker_postrouing_on_host "$docker_network_cidr"
done

#
# Configure routes to internet (our fake Internet) from DMZ nodes
#
dmz_nodes=("ids" "web" "proxy")
for container_name in "${dmz_nodes[@]}"
do
    reset_default_route "$container_name" "172.16.10.1"
done
#
# Configure routes to internet (our fake Internet) from intranet nodes
#
intranet_nodes=("hostA" "hostB" "hostC" "hostD")
for container_name in "${intranet_nodes[@]}"
do
    reset_default_route "$container_name" "172.16.12.1"
done
#
# Configure routes from internet
#
reset_default_route "foreign" "10.0.0.1"
#
# Delete default routes on routers
#
docker exec router1 ip route del default
docker exec router2 ip route del default

#
# router1 iptables configuration
#
# Disable input, output, and forward by default
docker exec router1 iptables -P INPUT DROP
docker exec router1 iptables -P OUTPUT DROP
docker exec router1 iptables -P FORWARD DROP
# Create chain to handle DMZ NAT postrouting rules for outgoing connections to internet
docker exec router1 iptables -t nat -N DMZ_POSTROUTING
docker exec router1 iptables -t nat -A POSTROUTING -j DMZ_POSTROUTING
docker exec router1 iptables -t nat -A DMZ_POSTROUTING -o eth1 -j MASQUERADE
# Allow forward from eth0 (dmznet) to eth1 (internet) for all connections
docker exec router1 iptables -A FORWARD -i eth0 -o eth1 -j ACCEPT
# Allow forward from eth1 (internet) to eth0 (dmznet) for ongoing and established connections
docker exec router1 iptables -A FORWARD -i eth1 -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT
# Create chain to handle DMZ NAT prerouting rules for incoming connections from internet to FTP and HTTP(S) services
docker exec router1 iptables -t nat -N DMZ_PREROUTING
docker exec router1 iptables -t nat -A PREROUTING -j DMZ_PREROUTING
docker exec router1 iptables -t nat -A DMZ_PREROUTING -i eth1 -p tcp --dport 21 -j DNAT --to 172.16.10.5:21
docker exec router1 iptables -t nat -A DMZ_PREROUTING -i eth1 -p tcp --dport 80 -j DNAT --to 172.16.10.5:80
docker exec router1 iptables -t nat -A DMZ_PREROUTING -i eth1 -p tcp --dport 443 -j DNAT --to 172.16.10.5:443
# Allow forward from eth1 (internet) to eth0 (dmznet) web for ports FTP, HTTP and HTTPS
docker exec router1 iptables -A FORWARD -i eth1 -o eth0 -p tcp -d 172.16.10.5 -m multiport --dports 21,80,443 -j ACCEPT

#
# router2
#
# Disable input, output, and forward by default
docker exec router2 iptables -P INPUT DROP
docker exec router2 iptables -P OUTPUT DROP
docker exec router2 iptables -P FORWARD DROP
# Allow forward from eth1 (intranet) hostA to eth0 (dmznet) web port 22 (SSH)
docker exec router2 iptables -A FORWARD -i eth1 -o eth0 -p tcp --dport 22 -d 172.16.10.5 -s 172.16.12.3 -j ACCEPT
# Allow forward from eth1 (intranet) all hosts to eth0 (dmznet) proxy port 8080
docker exec router2 iptables -A FORWARD -i eth1 -o eth0 -p tcp --dport 8080 -d 172.16.10.6 -j ACCEPT
# Allow forward from eth0 (dmznet) to eth1 (intranet) for ongoing and established connections
docker exec router2 iptables -A FORWARD -i eth0 -o eth1 -p tcp -m state --state RELATED,ESTABLISHED -j ACCEPT

#
# web
#
# Configue route to intranet
docker exec web ip route add 172.16.12.0/24 via 172.16.10.2

#
# proxy
#
# Configue route to intranet
docker exec proxy ip route add 172.16.12.0/24 via 172.16.10.2

#
# ids
#
# Mirror to ids all traffic from internet
docker exec router1 iptables -t mangle -A PREROUTING -i eth1 -d 10.0.0.1 -j TEE --gateway 172.16.10.4
# Allow send output traffic from mangle
docker exec router1 iptables -A OUTPUT -o eth0 -d 10.0.0.1 -j ACCEPT
